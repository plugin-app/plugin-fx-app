import 'bootstrap/dist/css/bootstrap.css';

const Footer = () => {
    return (
        <div className="footer fixed-bottom bg-dark">
            <div className="container">
                <div className="text-muted text-center">
                    © 2021 Copyright:
                    <span> PluginFx</span>
                </div>
            </div>
        </div>
    );
};

export default Footer;
