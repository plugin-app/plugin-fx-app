import { useSelector } from 'react-redux';
import defaultImage from '../../assets/logo192.png';
import { Link } from 'react-router-dom';
import './CardPlugin.scss';
import 'bootstrap/dist/css/bootstrap.css';

const CardPlugin = (props) => {
    const history = useSelector((state) => state.data);
    console.log(history);
    const plugins = useSelector((state) => state.plugin.all);
    const toFind = useSelector((state) => state.search.searched);
    let plugin = null;

    if (toFind.length) {
        plugin = plugins.filter((plugin) =>
            plugin.name.toLowerCase().includes(toFind.toLowerCase())
        );
    } else {
        plugin = plugins;
    }

    return (
        <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
            {plugin.map((p) => (
                <div className="col main-vcard" key={p._id}>
                    <div className="card shadow-sm container-fluid">
                        <img
                            src={p.image || defaultImage}
                            alt={p.name}
                            className="card__image"
                        />
                        <div className="card-body">
                            <h3 className="card-text">{p.name}</h3>
                            <p className="card-text">{p.description}</p>
                            <div className="d-flex justify-content-between align-items-center">
                                <div className="btn-group">
                                    <Link
                                        to={{
                                            pathname: '/detail',
                                            id: p._id,
                                        }}
                                    >
                                        <button
                                            type="buttom"
                                            className="btn btn-sm btn-outline-secondary"
                                        >
                                            View
                                        </button>
                                    </Link>
                                </div>
                                <small className="text-muted">9</small>
                            </div>
                        </div>
                    </div>
                </div>
            ))}
        </div>
    );
};

export default CardPlugin;
