import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getAllPluginsAsync } from '../../redux/slices/plugins.slice';
import { useLocation } from 'react-router-dom';
import defaultImage from '../../assets/logo192.png';
import 'bootstrap/dist/css/bootstrap.css';
import './PluginDetail.scss';

const PluginDetail = (state) => {
    const location = useLocation();
    const plugins = useSelector((state) => state.plugin.all);
    const dispatch = useDispatch();

    useEffect(() => {
        if (!plugins.length) dispatch(getAllPluginsAsync());
    });

    const id = location.id;
    const pluginDetail = plugins.find((plugins) => plugins._id === id);

    return (
        <div className="col plugin-detail" key={pluginDetail._id}>
            <div className="card shadow plugin-detail__card d-flex justify-content-center">
                <div className="card-body plugin-detail__card-body">
                    <img
                        src={pluginDetail.image || defaultImage}
                        alt={pluginDetail.name}
                        className="detail-image"
                    />
                    <div className="d-flex  flex-column shadow-sm plugin-detail__main-info">
                        <h3 className="card-text">{pluginDetail.name}</h3>
                        <div className="group">
                            <h4>Developer:</h4>
                            <p className="card-text">
                                {pluginDetail.developer}
                            </p>
                        </div>
                        <div className="btn__description">
                            <h4>Description:</h4>
                            <p className="card-text">
                                {pluginDetail.description}
                            </p>
                        </div>
                    </div>
                    <div className="btn">
                        <span>
                            <h5>Category:</h5>
                            {pluginDetail.category}
                        </span>
                        <span>
                            <h5>Type:</h5>
                            {pluginDetail.pluginType}
                        </span>
                        <span>
                            <h5>Platform:</h5>
                            {pluginDetail.platform}
                        </span>
                        <span>
                            <h5>Format:</h5>
                            {pluginDetail.format}
                        </span>
                        <span className="alert-success">
                            <h5
                                style={{
                                    'text-transform': 'uppercase',
                                    'margin-top': '1rem',
                                }}
                            >
                                {pluginDetail.comercial}
                            </h5>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PluginDetail;
