import { useHistory } from 'react-router-dom';

const Home = (props) => {
    const history = useHistory();
    if (props.user) history.push('/');

    return (
        <div class="px-4 pt-5 my-5 text-center border-bottom">
            <h1 class="display-4 fw-bold">Plugin Fx</h1>
            <div class="col-lg-6 mx-auto">
                <p class="lead mb-4">
                    Find info about audio effects and audio procesors.
                </p>
                <div class="d-grid gap-2 d-sm-flex justify-content-sm-center mb-5"></div>
            </div>
        </div>
    );
};

export default Home;
