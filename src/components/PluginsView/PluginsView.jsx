import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getAllPluginsAsync } from '../../redux/slices/plugins.slice';
import CardPlugin from '../CardPlugin/CardPlugin';
import 'bootstrap/dist/css/bootstrap.css';

const PluginsView = () => {
    const plugins = useSelector((state) => state.plugin.all);
    const dispatch = useDispatch();

    useEffect(() => {
        if (!plugins.length) dispatch(getAllPluginsAsync());
    });

    return (
        <div className=" album py-5">
            <div className="container">
                <CardPlugin />
            </div>
        </div>
    );
};

export default PluginsView;
