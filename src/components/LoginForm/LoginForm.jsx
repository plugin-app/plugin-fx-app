import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { loginAsync } from '../../redux/slices/user.slice';
import 'bootstrap/dist/css/bootstrap.css';
import './LoginForm.scss';

const INITIAL_STATE = {
    email: '',
    password: '',
};

const LoginForm = (props) => {
    const [formData, setFormData] = useState(INITIAL_STATE);
    const { error, hasUser } = useSelector((state) => state.user);
    const dispatch = useDispatch();

    const handleFormSubmit = async (ev) => {
        ev.preventDefault();
        await dispatch(loginAsync(formData));
        setFormData(INITIAL_STATE);
    };

    if (hasUser) props.history.push('/');

    const handleInputChange = (ev) => {
        const { name, value } = ev.target;
        setFormData({ ...formData, [name]: value });
    };

    return (
        <div className="shadow p-3 mb-5 bg-body rounded login-container">
            <h2>Login</h2>
            <form onSubmit={handleFormSubmit}>
                <div className="row mb-3">
                    <label htmlFor="email" className="col-sm-2 col-form-label">
                        Email
                    </label>
                    <div className="col-sm-10">
                        <input
                            type="email"
                            name="email"
                            className="form-control"
                            id="email"
                            onChange={handleInputChange}
                            value={formData.email}
                        />
                    </div>
                </div>
                <div className="row mb-3">
                    <label
                        htmlFor="password"
                        className="col-sm-2 col-form-label"
                    >
                        Password
                    </label>
                    <div className="col-sm-10">
                        <input
                            type="password"
                            name="password"
                            className="form-control"
                            id="password"
                            onChange={handleInputChange}
                            value={formData.password}
                        />
                    </div>
                </div>
                <button type="submit" className="btn btn-primary">
                    Sign in
                </button>
            </form>
            {error && <div className="alert alert-danger">{error}</div>}
        </div>
    );
};

export default LoginForm;
