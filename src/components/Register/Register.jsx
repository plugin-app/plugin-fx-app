import { useState } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { registerAsync } from '../../redux/slices/user.slice';
import 'bootstrap/dist/css/bootstrap.css';
import './Register.scss';

const INITIAL_STATE = {
    username: ' ',
    email: ' ',
    password: ' ',
};

const Register = (props) => {
    const [formFields, setFormFields] = useState(INITIAL_STATE);
    const { error, hasUser } = useSelector((state) => state.user);
    const dispatch = useDispatch();

    const handleFormSubmit = async (ev) => {
        ev.preventDefault();
        await dispatch(registerAsync(formFields));
        setFormFields(INITIAL_STATE);
    };

    if (hasUser) props.history.push('/');

    const handleInputChange = (ev) => {
        const { name, value } = ev.target;
        setFormFields({ ...formFields, [name]: value });
    };

    return (
        <div className="shadow p-3 mb-5 bg-body rounded register-container">
            <h2>Sign Up</h2>
            <form onSubmit={handleFormSubmit}>
                <div className="row mb-3">
                    <label
                        htmlFor="username"
                        className="col-sm-2 col-form-label"
                    >
                        Name
                    </label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            name="username"
                            id="username"
                            onChange={handleInputChange}
                            value={formFields.username}
                        />
                    </div>
                </div>
                <div className="row mb-3">
                    <label htmlFor="email" className="col-sm-2 col-form-label">
                        Email
                    </label>
                    <div className="col-sm-10">
                        <input
                            type="email"
                            className="form-control"
                            name="email"
                            id="email"
                            onChange={handleInputChange}
                            value={formFields.email}
                        />
                    </div>
                </div>
                <div className="row mb-3">
                    <label
                        htmlFor="password"
                        className="col-sm-2 col-form-label"
                    >
                        Password
                    </label>
                    <div className="col-sm-10">
                        <input
                            type="password"
                            className="form-control"
                            name="password"
                            id="password"
                            onChange={handleInputChange}
                            value={formFields.password}
                        />
                    </div>
                </div>
                <button type="submit" className="btn btn-primary">
                    Sign in
                </button>
            </form>
            {error && <div className="alert alert-danger">{error}</div>}
        </div>
    );
};

export default Register;
