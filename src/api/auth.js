export const serverDirection = 'https://audio-fx-plugin.herokuapp.com';
const registerUrl = `${serverDirection}/auth/register`;
const loginUrl = `${serverDirection}/auth/login`;
const logoutUrl = `${serverDirection}/auth/logout`;
const checkSessionUrl = `${serverDirection}/auth/check-session`;

export const register = async (userData) => {
    const request = await fetch(registerUrl, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
        },
        credentials: 'include',
        withCredentials: true,
        body: JSON.stringify(userData),
    });
    const response = await request.json();

    if (!request.ok) {
        throw new Error(response.message);
    }

    return response;
};

export const login = async (userData) => {
    const request = await fetch(loginUrl, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
        },
        credentials: 'include',
        withCredentials: true,
        body: JSON.stringify(userData),
    });
    const response = await request.json();
    console.log(response.message);
    if (!request.ok) {
        throw new Error(response.message);
    }

    return response;
};

export const logout = async () => {
    const request = await fetch(logoutUrl, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
        },
        credentials: 'include',
        withCredentials: true,
    });
    const response = await request.json();
    return response;
};

export const checkSession = async () => {
    const request = await fetch(checkSessionUrl, {
        method: 'GET',
        headers: {
            Accept: 'application/json',

            'Access-Control-Allow-Origin': '*',
        },
        credentials: 'include',
        withCredentials: true,
    });
    const response = await request.json();
    return response;
};
