import React from 'react';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { checkSessionAsync } from './redux/slices/user.slice';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import {
    Header,
    LoginForm,
    Register,
    Footer,
    Home,
    PluginsView,
    PluginFormCreate,
    SecureRoute,
    PluginDetail,
} from './components';
import './App.scss';

const App = () => {
    const dispatch = useDispatch();
    useEffect(() => {
        getUser();
    });

    console.log('hola');

    const getUser = async () => {
        dispatch(checkSessionAsync());
    };

    return (
        <Router>
            <div className="app">
                <Header />
                <Switch>
                    <Route exact path="/" component={() => <Home />} />
                    <Route
                        exact
                        path="/plugins"
                        component={() => <PluginsView />}
                    />
                    <Route
                        exact
                        path="/login"
                        component={(props) => <LoginForm {...props} />}
                    />
                    <Route
                        exact
                        path="/register"
                        component={(props) => <Register {...props} />}
                    />
                    <SecureRoute
                        exact
                        path="/create"
                        component={(props) => <PluginFormCreate {...props} />}
                    />
                    <Route
                        exact
                        path="/detail"
                        component={(props) => <PluginDetail {...props} />}
                    />
                </Switch>
                <Footer />
            </div>
        </Router>
    );
};

export default App;
