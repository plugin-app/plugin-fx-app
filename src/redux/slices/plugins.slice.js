import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { getAllPlugins } from '../../api/plugins';

const INITIAL_STATE = {
    all: [],
};

export const getAllPluginsAsync = createAsyncThunk(
    'plugins/getAllPlugins',
    async () => {
        return await getAllPlugins();
    }
);

export const pluginsSlice = createSlice({
    name: 'plugins',
    initialState: INITIAL_STATE,
    reducers: {
        addPlugin: (state, action) => {
            state.all = [...state.all, action.payload];
        },
    },
    extraReducers: (builder) => {
        builder.addCase(getAllPluginsAsync.fulfilled, (state, action) => {
            state.all = action.payload;
        });
    },
});

export const { addPlugin } = pluginsSlice.actions;
