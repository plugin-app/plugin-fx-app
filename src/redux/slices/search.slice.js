import { createSlice } from '@reduxjs/toolkit';

const INTIAL_STATE = {
    searched: '',
};

export const searchSlice = createSlice({
    name: 'search',
    initialState: INTIAL_STATE,
    reducers: {
        handleInput: (state, action) => {
            const { value } = action.payload;
            state.searched = value;
        },
    },
});
export const { handleInput } = searchSlice.actions;
