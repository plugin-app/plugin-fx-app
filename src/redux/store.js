import { configureStore } from '@reduxjs/toolkit';
import { userSlice } from './slices/user.slice';
import { pluginsSlice } from './slices/plugins.slice';
import { formSlice } from './slices/form.slice';
import { searchSlice } from './slices/search.slice';

export default configureStore({
    reducer: {
        user: userSlice.reducer,
        plugin: pluginsSlice.reducer,
        form: formSlice.reducer,
        search: searchSlice.reducer,
    },
});
